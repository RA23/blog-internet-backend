<?php

namespace App\Http\Controllers;

use App\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TicketController extends Controller
{

    public function own($id)
    {
        // Devolvemos todas las Entradas del Usuario
        $tickets = Ticket::where('user_id', $id)->get();
        return response()->json($tickets);
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // Devolvemos todas las Entradas
        $tickets = Ticket::all();
        return response()->json($tickets);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data = $request->all();
        
        // Validaciones
        $validator = Validator::make($data, [
            'titulo' => 'required|string|max:255',
            'autor' => 'required|string|max:150',
            'fecha_publicacion' => 'required',
            'contenido' => 'required',
        ]);

        // Devolvemos un error de validación en caso de fallo en las verificaciones
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 422);
        }

        // Creamos una Entrada
        $ticket = Ticket::create([
            'titulo' => $data['titulo'],
            'autor' => $data['autor'],
            'fecha_publicacion' => $data['fecha_publicacion'],
            'contenido' => $data['contenido'],
            'user_id' => $data['user_id']
        ]);

        return response()->json(['message' => 'Entrada Creada']);
    }

    /**
     * Display the specified resource.
     */
    public function show(Ticket $ticket)
    {
        return response()->json($ticket);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Ticket $ticket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Ticket $ticket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Ticket $ticket)
    {
        $ticket->delete();

        return response()->json([
            'message'=>'Entrada Eliminada'
        ]);
    }
}
